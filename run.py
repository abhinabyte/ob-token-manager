import time
import uuid
import requests
import urllib.parse
from flask import Flask, request
from jwcrypto import jwk, jwt

KID = "7CzPrKwRhyav66l4OEz_Z8jU2PI"

ROCK_CLIENT_ID = "0cd6cdb3-58c9-4399-bfb0-16c83e267110"

ACCOUNT_REQUEST_ID = "A70ee7549-51eb-43c4-a9c1-3ed3a9dfbc8b"

REDIRECT_URL =  "https://app.getpostman.com/oauth2/callback"

PRIVATE_RSA_KEY = """
-----BEGIN PRIVATE KEY-----
MIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQC9IxaaxFY0NQy5
f+R26sjFyDhcQzbF3bXxuNM8yVMLFIGrtV4BsS6IalICdmYsAE0bgVE19/uhJVwp
gygOhga+3X6OUJ4atrX7NzMjR0UAmnMwZEK0dyN/rgKSp/Sch1vjxAXw7nsYMv7X
0QA/m25YAebhBfMN+qxG/PY0h4dOvTLDoHnkYnIMa9uwVpL8dlMx9I3AWhgP75YV
vxut8fN3l1LoT5Kdoswu5odlh2ElXaXwgotX3CPQIWRHwzrp/oTv6dRkNG26xHBq
m/NaQFuP+ZhJZeG3y0Lh5S99QHVsAUBvu1Nl0N3KxqDpwkMfl1okb1oGHzh8G96f
W3IGii/VAgMBAAECggEAW4fzlIqXvUQ/GDaQdgxBhZXT0LKmezNErepoL+6uRZpy
9oobKG56uoHZll9+KqUqq7j1eEy+dcoCxAQ5Cm5KLWw7ptvU59CsWRrpoJUJC7Wz
MSQUNAnEIDIyBI9LjcTj72IaDfMblyZ2tjLrD91ElUq2VQfwDD/Xx7L744gDL1MC
Df7QAGGZ8BMlxk4EyRBd9pCtmKBvscMcnWcHDADcAHofEXgsI6CNeBwCPCZA0A4N
fGNCjOJZPqg57wmym2cDYHcoyCh1EdRGVtZrAzQrWbwyTL9G/eViV0sTuuTAO2kz
p6rA4y/rOFvzfIDLQZGdo4uto8g0XU4MN3/n9vvbAQKBgQD4HmfPNGckMy1b0CRU
4g0Q+zCv17g9gJzqpkis4g3WjGvrjGQDy7TuFmh7HCrhRVHYpaqvUaTPfjG/dCIF
Y5AL32KK8I48/cSCf+6sstQgTnLy05guhiYDEhBVw53OstGB7rCmrnnsxKqyjgu/
DNEdtKgtvNmqoB75tyAhbHBMZQKBgQDDJRGuRxoeeIFAzvT4Vt/jI4BB7AGEJLCr
PM7UaPdCQ2x/QSDh3NYgy1kPnF23+HbBOtKzwwVEnuU15WPqe3kra21JQ3e4WwgQ
cCz0T2qQcdzRntv5BS8UH2gFO0MO0FtOZe8QsSJTyUFzBR5BKBORmWJ+dbcjE1b+
XmZCoX8GsQKBgBtFJD220bs90LdtmgCmU69RHFnHSN2hnB2CJyyxL7or9tSNupRc
OEeOuO8G+oOF3egE1UytmYKgqREUz4RuG+/iHm+M3uU/MvDw5KeghAYE+UdNEEXU
wqrFunviox6LstEoDcxUEtx+BRDY4dueqgEs0Ost/XJuZNknUmW6CGGtAoGATZsd
V/Cg24NS68IiO+zYps0EgbLou3OX6tGI84I26txDdqwg9pHLeklERugJgVNHbvsE
tkaVfr+uKevq+2JVQsY7NQqmvuM85sSlrBc1KvKNPbPuiS7dlSQUSxhRR7/mv9vb
4gh/vVBpPePIL2NPvPSB7gbEaSeUzh4ejtsI2FECgYBuEFIwdmwfVKZTYUc2swYr
aiesvOTUPZ1O4DTILat1C2B5Obhqi6ajkcxtTBugYizWrbAIFDQop9z1+P3x2Njz
srJ0Ty5RZelcCFfI7aguJErEanDMeViLgkKXKkNM5fl6wVZl/MVw8Kvr8tKS3C52
WoJedbnkHJcpt8/5gyd4LQ==
-----END PRIVATE KEY-----
"""

app = Flask(__name__)

@app.route("/v1/auth-url")
def get_auth_url():
    request_token = get_request_token(KID, ROCK_CLIENT_ID, ACCOUNT_REQUEST_ID)
    auth_endpoint = authorise_url(request_token, ROCK_CLIENT_ID)
    return auth_endpoint

@app.route("/v1/oauth2/callback")
def get_access_token_from_callback():
    authcode = request.args.get('code', type = str)
    signed_token = get_client_authentication_jwt(KID, ROCK_CLIENT_ID)
    access_token = get_access_token(signed_token, authcode, REDIRECT_URL)
    return access_token

def get_request_token(kid: str, forge_rock_client_id: str, account_request_id: str) -> str:
    jwt_iat = int(time.time())
    jwt_exp = jwt_iat + 3600
    header = dict(alg='RS256', kid=kid, typ='JWT')
    claims = dict(
        dict(id_token=dict(
            acr=dict(
                value="urn:openbanking:psd2:sca",
                essential=True
            ),
            openbanking_intent_id=dict(
                value=account_request_id,
                essential=True
            )
        )
        ),
        userinfo=dict(
            openbanking_intent_id=dict(
                value=account_request_id,
                essential=True
            )
        )

    )
    data = dict(
        claims=claims,
        aud="https://matls.as.aspsp.ob.forgerock.financial/oauth2/openbanking",
        scope="accounts openid",
        iss=forge_rock_client_id,
        client_id=forge_rock_client_id,
        response_type="code id_token",
        redirect_uri="https://app.getpostman.com/oauth2/callback",
        state="5a6b0d7832a9fb4f80f1170a",
        nonce="5a6b0d7832a9fb4f80f1170a",
        jti=str(uuid.uuid4()),
        exp=jwt_exp,
        iat=jwt_iat
    )

    token = jwt.JWT(header=header, claims=data)
    key_obj = jwk.JWK.from_pem(PRIVATE_RSA_KEY.encode('UTF-8'))
    token.make_signed_token(key_obj)
    signed_token = token.serialize()
    return signed_token

def authorise_url(request_token: str, forge_rock_client_id: str) -> str:
    params = urllib.parse.urlencode(
        ([('client_id', forge_rock_client_id),
          ('redirect_uri', "https://app.getpostman.com/oauth2/callback"),
          ('response_type', 'code id_token'),
          ('scope', "accounts openid"),
          ('request', request_token),
          ]))

    pre = requests.Request('GET', "https://matls.as.aspsp.ob.forgerock.financial/oauth2/realms/root/realms/openbanking/authorize", params=params).prepare()
    return pre.url

def get_client_authentication_jwt(kid:str, forge_rock_client_id: str) -> str:
    jwt_iat = int(time.time())
    jwt_exp = jwt_iat + 60
    header = dict(alg='RS256', kid=kid, typ='JWT')
    claims = dict(
        exp=jwt_exp,
        sub=forge_rock_client_id,
        iss=forge_rock_client_id,
        aud="https://matls.as.aspsp.ob.forgerock.financial/oauth2/openbanking",
    )
    token = jwt.JWT(header=header, claims=claims)
    key_obj = jwk.JWK.from_pem(PRIVATE_RSA_KEY.encode('UTF-8'))
    token.make_signed_token(key_obj)
    signed_token = token.serialize()
    return signed_token

def get_access_token(signed_token: str, authorization_code: str, redirect_uri: str) -> str:
    data_dict = dict(
        grant_type='authorization_code',
        code=authorization_code,
        redirect_uri=redirect_uri,
        client_assertion_type='urn:ietf:params:oauth:client-assertion-type:jwt-bearer',
        client_assertion=signed_token
    )

    client = ('./transport.pem', './transport.key')
    response = requests.post(
        'https://matls.as.aspsp.ob.forgerock.financial/oauth2/realms/root/realms/openbanking/access_token',
        data=data_dict,
        verify=False,
        cert=client
    )

    return response.json().get('access_token')

if __name__ == '__main__':
    app.run(port=8080)
